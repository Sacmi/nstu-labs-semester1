#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int *getArray(int &size)
{
    int *array;

    while (true)
    {
        printf_s("Введите длину массива: ", &size);
        scanf_s("%d", &size);

        if (size > 1)
            break;

        printf_s("Вы ввели неверные данные. Попробуйте еще раз.\n");
    }

    array = (int *)malloc(sizeof(int) * size);

    printf_s("Введите элементы массивов (допустимы только целые числа): ");
    for (size_t i = 0; i < size; i++)
        scanf_s("%d", &array[i]);

    return array;
}

void printSpecialElements(int *array, int size)
{
    if (array[0] > array[1])
        printf_s("%d ", array[0]);

    for (size_t i = 1; i < size - 1; i++)
    {
        if (array[i] > array[i - 1] && array[i] > array[i + 1])
            printf_s("%d ", array[i]);
    }

    if (size > 2 && array[size - 1] > array[size - 2])
        printf_s("%d", array[size - 1]);
}

int main()
{
    int *ptr, size;

    setlocale(LC_ALL, "Russian");

    ptr = getArray(size);

    printf_s("Вывод элементов, которые больше своих соседей: \n");
    printSpecialElements(ptr, size);

    return 0;
}